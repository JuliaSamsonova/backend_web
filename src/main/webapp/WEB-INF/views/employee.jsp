<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>System IdUsers</title>
</head>
<body>
<div>
    <h1>System IdUsers</h1>
</div>

<table border="1">
    <tr>
        <td>EmployeeId</td>
        <td>Username</td>
        <td>Age</td>
        <td>Login</td>
        <td>Password</td>
        <td>BirthDay</td>
        <td>HavePet</td>
    </tr>
    <tr>
        <td>${employee.id}</td>
        <td>${employee.username}</td>
        <td>${employee.age}</td>
        <td>${employee.login}</td>
        <td>${employee.password}</td>
        <td>${employee.birthDay}</td>
        <td>${employee.havePet}</td>
    </tr>
</table>
</body>
</html>
package initial;

import initial.dao.EmployeeDao;
import initial.dao.jdbc.EmployeeDaoImpl;
import initial.domain.Employee;
import initial.domain.TestSpring;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import java.net.http.HttpClient;

public class SpringDemo {

    private static final Logger log = Logger.getLogger(SpringDemo.class);

    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext("initial");

//        TestSpring testSpring = (TestSpring)context.getBean("testSpring");
//        HttpClient httpClient = (HttpClient)context.getBean("httpClient");
//            System.out.println(testSpring + " ");
//            System.out.println(httpClient + " ");

//      DataSource dataSource = (DataSource) context.getBean("generateSource"); //вызов напрямую через DataSource
//      DataSource dataSource = context.getBean(DataSource.class);

//        EmployeeDao employeeDaoImpl = (EmployeeDao) context.getBean("employeeDaoImpl"); // вызов из EmployeeDaoImpl с DriverManager
//        замененным на dataSource
//        EmployeeDao employeeDaoByClassName = context.getBean(EmployeeDaoImpl.class);
//        String login = employeeDaoImpl.findAll().get(2).getLogin();
//        String login1 = employeeDaoByClassName.findAll().get(2).getLogin();
//        System.out.println(login, login1);
//

        EmployeeDao employeeRepositoryJDBCTemplate = (EmployeeDao)context.getBean("employeeRepositoryJDBCTemplate"); //вызов из EmployeeDao
//        с применением JdbcTemplate, NamedParameterJdbcTemplate
        for(Employee employee:employeeRepositoryJDBCTemplate.findAll()){
            log.info(employee.getLogin());

        }
    }
}

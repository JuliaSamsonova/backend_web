package initial.dao;
import initial.domain.Employee;
import initial.exceptions.ResourceNotFoundException;
import initial.util.DatabaseConfiguration;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class EmployeeDaoImpl implements EmployeeDao {

    private static final String DATABASE_DRIVER_NAME = "driverName";
    private static final String DATABASE_URL = "url";
    private static final String DATABASE_LOGIN = "login";
    private static final String DATABASE_PASSWORD = "password";
    private static final String DATABASE_INITIAL_SIZE = "initialSize";

    public static DatabaseConfiguration configuration = DatabaseConfiguration.getInstance();

    private static final String EMPLOYEES_ID = "id";
    private static final String EMPLOYEES_USERNAME = "userName";
    private static final String EMPLOYEES_AGE = "age";
    private static final String EMPLOYEES_LOGIN = "login";
    private static final String EMPLOYEES_PASSWORD = "password";
    private static final String EMPLOYEES_BIRTH_DAY = "birth_day";
    private static final String EMPLOYEES_HAVE_PET = "have_pet";

    private DataSource dataSource;

    public EmployeeDaoImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public List<Employee> findAll() {

//        String driverName = configuration.getProperty(DATABASE_DRIVER_NAME);
//        String url = configuration.getProperty(DATABASE_URL);
//        String login = configuration.getProperty(DATABASE_LOGIN);
//        String password = configuration.getProperty(DATABASE_PASSWORD);
//        String initialSize = configuration.getProperty(DATABASE_INITIAL_SIZE);

        final String findAllQuery = "SELECT* FROM m_employees ORDER BY id desc";

//        try {
//            Class.forName(driverName);
//        } catch (ClassNotFoundException e) {
//            System.out.println("Exception was found");
//        }

        List<Employee> resultList = new ArrayList<Employee>();
        try (Connection connection = dataSource.getConnection();
             PreparedStatement prepareStatement = connection.prepareStatement(findAllQuery)) {
            ResultSet resultSet = ((PreparedStatement) prepareStatement).executeQuery();
            while (resultSet.next()) {
                resultList.add(parseResultSet(resultSet));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return resultList;
    }

    private Employee parseResultSet(ResultSet resultSet) throws SQLException {
        Employee employee = new Employee();
        employee.setId(resultSet.getLong(EMPLOYEES_ID));
        employee.setUsername(resultSet.getString(EMPLOYEES_USERNAME));
        employee.setAge(resultSet.getInt(EMPLOYEES_AGE));
        employee.setLogin(resultSet.getString(EMPLOYEES_LOGIN));
        employee.setPassword(resultSet.getString(EMPLOYEES_PASSWORD));
        employee.setBirthDay(resultSet.getDate(EMPLOYEES_BIRTH_DAY));
        employee.isHavePet(resultSet.getBoolean(EMPLOYEES_HAVE_PET));
        return employee;
    }


    @Override
    public List<Employee> search(String searchParam) {
        String driverName = configuration.getProperty(DATABASE_DRIVER_NAME);
        String url = configuration.getProperty(DATABASE_URL);
        String login = configuration.getProperty(DATABASE_LOGIN);
        String password = configuration.getProperty(DATABASE_PASSWORD);
        String initialSize = configuration.getProperty(DATABASE_INITIAL_SIZE);

        final String searchForAllPrepared = "SELECT* FROM m_employees WHERE id > ? ORDER BY id asc";

        try {
            Class.forName(driverName);
        } catch (ClassNotFoundException e) {
            System.out.println("Exception was found");
        }
        List<Employee> resultList = new ArrayList<Employee>();
        try (Connection connection = DriverManager.getConnection(url, login, password);
             PreparedStatement prepareStatement = connection.prepareStatement(searchForAllPrepared)) {
            prepareStatement.setLong(1, Long.parseLong(searchParam));
            ResultSet resultSet = ((PreparedStatement) prepareStatement).executeQuery();
            while (resultSet.next()) {
                resultList.add(parseResultSet(resultSet));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return resultList;
    }

    @Override
    public Optional<Employee> findById(Long employeeId) {
        return Optional.ofNullable(findOne(employeeId));
    }

    @Override
    public Employee update(Employee employee) {
        String driverName = configuration.getProperty(DATABASE_DRIVER_NAME);
        String url = configuration.getProperty(DATABASE_URL);
        String login = configuration.getProperty(DATABASE_LOGIN);
        String password = configuration.getProperty(DATABASE_PASSWORD);
        String initialSize = configuration.getProperty(DATABASE_INITIAL_SIZE);

        final String updateQuery = "UPDATE m_employees SET userName =?, age = ?, login= ?, password = ?, " +
                "birth_day = ?, have_pet =? WHERE id = ?";

        try {
            Class.forName(driverName);
        } catch (ClassNotFoundException e) {
            System.out.println("Exception was found");
        }
        try (Connection connection = DriverManager.getConnection(url, login, password);
             PreparedStatement prepareStatement = connection.prepareStatement(updateQuery)) {
            prepareStatement.setString(1, employee.getUsername());
            prepareStatement.setInt(2, employee.getAge());
            prepareStatement.setString(3, employee.getLogin());
            prepareStatement.setString(4, employee.getPassword());
            prepareStatement.setDate(5, employee.getBirthDay());
            prepareStatement.setBoolean(6, employee.isHavePet(true));
            prepareStatement.setLong(7, employee.getId());
            prepareStatement.executeUpdate();
            return findOne(employee.getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return employee;
    }


    @Override
    public Employee delete(Employee employee) {
        String driverName = configuration.getProperty(DATABASE_DRIVER_NAME);
        String url = configuration.getProperty(DATABASE_URL);
        String login = configuration.getProperty(DATABASE_LOGIN);
        String password = configuration.getProperty(DATABASE_PASSWORD);
        String initialSize = configuration.getProperty(DATABASE_INITIAL_SIZE);

        final String deleteQuery = "DELETE FROM m_employees WHERE id = ?";

        try {
            Class.forName(driverName);
        } catch (ClassNotFoundException e) {
            System.out.println("Exception was found");
        }
        ResultSet resultSet = null;
        try (Connection connection = DriverManager.getConnection(url, login, password);
             PreparedStatement prepareStatement = connection.prepareStatement(deleteQuery)) {
            prepareStatement.setLong(1, employee.getId());
            prepareStatement.executeQuery();
            return null;
        } catch (SQLException e) {
            throw new RuntimeException("Some issues with DELETE", e);
        }
    }


    @Override
    public Employee save(Employee employee) {
        String driverName = configuration.getProperty(DATABASE_DRIVER_NAME);
        String url = configuration.getProperty(DATABASE_URL);
        String login = configuration.getProperty(DATABASE_LOGIN);
        String password = configuration.getProperty(DATABASE_PASSWORD);
        String initialSize = configuration.getProperty(DATABASE_INITIAL_SIZE);

        final String insertQuery = "INSERT INTO m_employees (userName, age, login, password, " +
                "birth_day, have_pet) VALUES( ?, ?, ?, ?, ?, ?)";

        try {
            Class.forName(driverName);
        } catch (ClassNotFoundException e) {
            System.out.println("Exception was found");
        }
        try (Connection connection = DriverManager.getConnection(url, login, password);
             PreparedStatement prepareStatement = connection.prepareStatement(insertQuery)) {
            PreparedStatement lastInsertedId = connection.prepareStatement("SELECT currval('m_employees_id_seq') as last_inserted_id");
            prepareStatement.setString(1, employee.getUsername());
            prepareStatement.setInt(2, employee.getAge());
            prepareStatement.setString(3, employee.getLogin());
            prepareStatement.setString(4, employee.getPassword());
            prepareStatement.setDate(5, employee.getBirthDay());
            prepareStatement.setBoolean(6, employee.isHavePet(true));
            prepareStatement.executeUpdate();
            ResultSet resultSet = lastInsertedId.executeQuery();
            resultSet.next();
            Long lastId = resultSet.getLong("last_inserted_id");
            return findOne(lastId);
        } catch (SQLException e) {
            throw new RuntimeException("Some issues with operation of insertion", e);
        }
    }

    @Override
    public Employee findOne(Long employeeId) {
        String driverName = configuration.getProperty(DATABASE_DRIVER_NAME);
        String url = configuration.getProperty(DATABASE_URL);
        String login = configuration.getProperty(DATABASE_LOGIN);
        String password = configuration.getProperty(DATABASE_PASSWORD);
        String initialSize = configuration.getProperty(DATABASE_INITIAL_SIZE);

        final String findById = "SELECT* FROM m_employees WHERE id = ?";

        try {
            Class.forName(driverName);
        } catch (ClassNotFoundException e) {
            System.out.println("Exception was found");
        }
        Employee employee = null;
        ResultSet resultSet = null;
        try (Connection connection = DriverManager.getConnection(url, login, password);
             PreparedStatement prepareStatement = connection.prepareStatement(findById)) {
            prepareStatement.setLong(1, employeeId);
            resultSet = ((PreparedStatement) prepareStatement).executeQuery();
            if (resultSet.next()) {
                employee = parseResultSet(resultSet);
            } else {
                throw new ResourceNotFoundException("User with id" + employeeId + "was not found");
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                resultSet.close();
            } catch (SQLException throwable) {
                System.out.println(throwable.getMessage());
            }
        }
        return employee;
    }
}




package initial.dao.jdbcTemplate;

import initial.dao.EmployeeDao;
import initial.domain.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository ("employeeRepositoryJDBCTemplate")
public class EmployeeRepository implements EmployeeDao {

    private static final String EMPLOYEES_ID = "id";
    private static final String EMPLOYEES_USERNAME = "userName";
    private static final String EMPLOYEES_AGE = "age";
    private static final String EMPLOYEES_LOGIN = "login";
    private static final String EMPLOYEES_PASSWORD = "password";
    private static final String EMPLOYEES_BIRTH_DAY = "birth_day";
    private static final String EMPLOYEES_HAVE_PET = "have_pet";

    private JdbcTemplate jdbcTemplate;
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;


    public EmployeeRepository(JdbcTemplate jdbcTemplate, NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    @Override
    public List<Employee> findAll() {

        final String findAllQuery = "SELECT* FROM m_employees ORDER BY id desc";
        return jdbcTemplate.query(findAllQuery, this::parseRowMapper);
    }

    @Override
    public List<Employee> search(String searchParam) {
        final String searchQuery = "SELECT* FROM m_employees WHERE login=? ORDER by id desc";
        return jdbcTemplate.query(searchQuery, this::parseRowMapper, searchParam);

    }

    @Override
    public Optional<Employee> findById(Long employeeId) {
                return Optional.empty();
    }

    @Override
    public Employee update(Employee employee) {
        return null;
    }

    @Override
    public Employee delete(Employee employee) {
        return null;
    }

    @Override
    public Employee save(Employee employee) {
        final String insertQuery = "INSERT INTO m_employees (username, age, login, password, " +
                "birth_day, have_pet) VALUES( ?, ?, ?, ?, ?, ?)";
        final String getLastId = "SELECT currval('m_employees_id_seq') as last_inserted_id;";
        jdbcTemplate.update(insertQuery, employee.getUsername(), employee.getAge(), employee.getLogin(), employee.getPassword(),
                employee.getBirthDay(), employee.isHavePet());
        Long lastId = jdbcTemplate.queryForObject(getLastId, Long.class);
        return findOne(lastId);
    }

    @Override
    public Employee findOne(Long employeeId) {
        final String findById = "SELECT* FROM m_employees WHERE id:employeeId ORDER BY id desc";
        return (Employee) jdbcTemplate.query(findById,this::parseRowMapper, employeeId);
    }

    private Employee parseRowMapper (ResultSet resultSet,int i) throws SQLException {  //int i - имплементатор по колонкам
        Employee employee = new Employee();
        employee.setId(resultSet.getLong(EMPLOYEES_ID));
        employee.setUsername(resultSet.getString(EMPLOYEES_USERNAME));
        employee.setAge(resultSet.getInt(EMPLOYEES_AGE));
        employee.setLogin(resultSet.getString(EMPLOYEES_LOGIN));
        employee.setPassword(resultSet.getString(EMPLOYEES_PASSWORD));
        employee.setBirthDay(resultSet.getDate(EMPLOYEES_BIRTH_DAY));
        employee.setHavePet(resultSet.getBoolean(EMPLOYEES_HAVE_PET));
        return employee;
        }
    }


package initial.dao;

import initial.domain.Employee;
import java.util.List;
import java.util.Optional;

public interface EmployeeDao {
    /*CRUD operations*/
    /*Create = Insert*= save/
    /*Read = select*= findAll, search/
    /*Update*/
    /*Delete*/

    public List<Employee>findAll();
    public List <Employee> search (String searchParam);

    public Optional <Employee>findById (Long employeeId); // если не будет найден Employee, Optional <null>
    public  Employee update (Employee employee);
    public Employee delete (Employee employee);
    public  Employee save (Employee employee);
    public Employee findOne (Long employeeId); //должен возвратить Employee, иначе Exception



}

package initial.domain;

import org.springframework.stereotype.Component;

@Component
public class TestSpring {

    private NotBeanByDefault notBeanByDefault;

    public TestSpring(NotBeanByDefault notBeanByDefault) {
        this.notBeanByDefault = notBeanByDefault;
    }

   public NotBeanByDefault getNotBeanByDefault() {
        return notBeanByDefault;
    }

    public void setNotBeanByDefault(NotBeanByDefault notBeanByDefault) {
        this.notBeanByDefault = notBeanByDefault;
    }

    @Override
    public String toString() {
        return "TestSpring{" +
                 ", notBeanByDefault=" + notBeanByDefault +
                '}';
    }
}

package initial.domain;

import java.sql.Date;
import java.util.Objects;

public class Employee {
    private long id;
    private String username;
    private int age;
    private String login;
    private String password;
    private Date birthDay;
    private  boolean havePet;

    public Employee (){
            }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(Date birthDay) {
        this.birthDay = birthDay;
    }

    public boolean isHavePet(boolean aBoolean) {
        return havePet;
    }

    public void setHavePet(boolean havePet) {
        this.havePet = havePet;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return id == employee.id &&
                age == employee.age &&
                havePet == employee.havePet &&
                Objects.equals(username, employee.username) &&
                Objects.equals(login, employee.login) &&
                Objects.equals(password, employee.password) &&
                Objects.equals(birthDay, employee.birthDay);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, username, age, login, password, birthDay, havePet);
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", age=" + age +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", birthDay=" + birthDay +
                ", havePet=" + havePet +
                '}';
    }
}

package initial.service;

import initial.domain.Employee;

import java.util.List;
import java.util.Optional;

public interface EmplService {
    public List<Employee> findAll();
    public List <Employee> search (String searchParam);
    public Optional<Employee> findById (Long employeeId);
    public  Employee update (Employee employee);
    public Employee delete (Employee employee);
    public  Employee save (Employee employee);
    public Employee findOne (Long employeeId);

}

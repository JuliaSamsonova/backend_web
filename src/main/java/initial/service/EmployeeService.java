package initial.service;

import initial.dao.EmployeeDao;
import initial.domain.Employee;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EmployeeService implements EmplService {

        private EmployeeDao employeeDao;

        public EmployeeService(@Qualifier("employeeRepositoryJDBCTemplate") EmployeeDao employeeDao) {
            this.employeeDao = employeeDao;
        }

        public Employee findOne(Long employeeId) {
            return employeeDao.findOne(employeeId);
        }

        public List<Employee> findAll() {
            return employeeDao.findAll();
        }

    @Override
    public List<Employee> search(String searchParam) {
        return employeeDao.search(searchParam);
    }

    @Override
    public Optional<Employee> findById(Long employeeId) {
        return employeeDao.findById(employeeId);
    }

    @Override
    public Employee update(Employee employee) {
        return employeeDao.update(employee);
    }

    @Override
    public Employee delete(Employee employee) {
        return employeeDao.delete(employee);
    }

    @Override
    public Employee save(Employee employee) {
        return employeeDao.save(employee);
    }
}



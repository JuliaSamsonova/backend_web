package initial.aspect;

import org.apache.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.context.annotation.Configuration;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@Configuration
@Aspect
public class CalculationMethods {
    private static final org.apache.log4j.Logger log = Logger.getLogger(CalculationMethods.class);

    Map<String, Integer> method = new ConcurrentHashMap<>();

    public String display() {
        return method.entrySet().stream().map(e -> e.getKey() + " " + e.getValue()).collect(Collectors.joining(","));
    }

    @Pointcut("execution( * initial.dao.*.*.*(..))")
    public void aroundRepositoryPointcut() {
    }


    @Around("aroundRepositoryPointcut()")
    public Object calledMethods(ProceedingJoinPoint joinPoint) throws Throwable {
        log.info(method.computeIfAbsent(joinPoint.getSignature().getName(), (k -> 0)));
        Object proceed = joinPoint.proceed();
        log.info(method.computeIfPresent(joinPoint.getSignature().getName(), (k, v) -> v + 1));
        log.info("Number of called methods is" + " " + method);
        return proceed;
    }
}

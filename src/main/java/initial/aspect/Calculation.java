package initial.aspect;

import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.StopWatch;

@Configuration
@Aspect
public class Calculation {
    private static final Logger log = Logger.getLogger(Calculation.class);
    StopWatch stopWatch = new StopWatch();

    @Before("within(initial.dao.jdbcTemplate.EmployeeRepository)")
    public void logBefore(JoinPoint joinPoint) {
        log.info("Method" + joinPoint.getSignature().getName() + "was started");
    }

    @Pointcut("execution( * initial.dao.jdbcTemplate.EmployeeRepository.*(..))")
    public void aroundRepositoryPointcut() {
    }

    @Around("aroundRepositoryPointcut()")
    public Object getTime(ProceedingJoinPoint joinPoint) throws Throwable {
        log.info("Method" + joinPoint.getSignature().getName() + "was started");
        stopWatch.start();
        Object proceed = joinPoint.proceed();
        log.info("Method" + joinPoint.getSignature().getName() + "was finished");
        stopWatch.stop();
        log.info("Method executed" + joinPoint.getSignature().getName() + "in nanoseconds" + stopWatch.getTotalTimeNanos());
        return proceed;
    }
}

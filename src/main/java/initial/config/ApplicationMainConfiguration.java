package initial.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Import;

@Configuration
@ComponentScan("initial")
@Import({DataSourceConfiguration.class, ApplicationBeanConfiguration.class}) // lines 8-10 instead of application_context.xml
@EnableAspectJAutoProxy(proxyTargetClass = true) //create bean Calculation in ApplicationBeanConfiguration.class
////// that marked by annotation @Aspect in Calculation.class
public class ApplicationMainConfiguration {
}

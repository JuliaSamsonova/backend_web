package initial.config;

import initial.domain.NotBeanByDefault;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import java.net.http.HttpClient;

@Configuration
public class ApplicationBeanConfiguration {

    @Bean
    @Scope("singleton")
//   @Primary
    public NotBeanByDefault notBeanByDefault() {
        return new NotBeanByDefault();
    }

    @Bean
    @Scope("singleton")
    @Primary
    public NotBeanByDefault notBeanByDefault1() {
        return new NotBeanByDefault("Test application");
    }


    @Bean
    public HttpClient httpClient() {
        return HttpClient.newBuilder().version(HttpClient.Version.HTTP_2).build();
    }

//    @Bean
//    public MappingJackson2HttpMessageConverter getMessageConverter(){
//        return  new MappingJackson2HttpMessageConverter();
//    }


    // can be created by @EnableAspectJAutoProxy
//    @Bean("Calculation")
//    public Calculation getCalculation(){
//       return new Calculation();
//    }


    // can be created by @EnableAspectJAutoProxy
//    @Bean("Calculation")
//    public Calculation getCalculation(){
//       return new Calculation();
//    }


    /*(first version how HelloWeb-servlet.xml can be replaced by Annotations)*/
//    @Bean
//    public ViewResolver getViewResolver(ViewResolverRegistry registry) {
//        registry.jsp().prefix("/WEB-INF/jsp/").suffix(".jsp");
//
//        return new InternalResourceViewResolver();
//    }
}



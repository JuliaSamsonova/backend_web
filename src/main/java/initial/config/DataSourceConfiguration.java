//package initial.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import javax.sql.DataSource;

//@Data
//@Configuration
//@ConfigurationProperties("datasource")
//public class DataSourceConfiguration {
//
//    private String driverName;
//
//
//    private String url;
//
//
//    private String login;
//
//
//    private String password;
//
//
//    private Integer initialSize;


//    @Bean
//    public DataSource generateDatSource() {
//        /*legacy method*/
//        HikariConfig hikariConfig = new HikariConfig();
//        hikariConfig.setDriverClassName(driverName);
//        hikariConfig.setJdbcUrl(url);
//        hikariConfig.setUsername(login);
//        hikariConfig.setPassword(password);
//        hikariConfig.setMaxLifetime(initialSize);
//
//        return new HikariDataSource(hikariConfig);
//    }

//    @Bean
//    public JdbcTemplate getJdbcTemplate(DataSource dataSource) { // запрос к БД как обычно
//        // final String searchForAllPrepared = "SELECT* FROM m_employees WHERE id > ? ORDER BY id asc"
//        return new JdbcTemplate(dataSource);
//    }
//
//    @Bean
//    public NamedParameterJdbcTemplate getNamedParameter(DataSource dataSource) {   // в запросе к БД заменяем ? на :{имя параметра}
//        //final String searchForAllPrepared = "SELECT* FROM m_employees WHERE id > :{employeeId} ORDER BY id asc"
//        return new NamedParameterJdbcTemplate(dataSource);
//    }

//}


package initial.filter;

import org.apache.commons.lang3.StringUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public class SecondFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request,
                         ServletResponse response,
                         FilterChain filterChain) throws IOException, ServletException {

        System.out.println("This is SecondFilter");
        if (StringUtils.isNotBlank(((HttpServletRequest)request).getHeader("Customer Header_X"))){
            System.out.println("Custom Header was found!!!");
        }
            filterChain.doFilter(request,response);
    }

    @Override
    public void destroy() {

    }
}

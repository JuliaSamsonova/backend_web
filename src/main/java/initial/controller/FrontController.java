package initial.controller;

import initial.dao.EmployeeDao;
import initial.dao.EmployeeDaoImpl;
import initial.domain.Employee;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Date;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import static java.lang.Long.parseLong;

public class FrontController extends HttpServlet {
    private static final String FIND_ONE = "findOne";
    private static final String FIND_BY_ID = "findById";
    private static final String FIND_ALL = "findAll";
    private static final String UPDATE = "update";
    private static final String DELETE = "delete";
    private static final String SAVE = "save";
    private static final String SEARCH = "search";

    private EmployeeDao employeeDao = new EmployeeDaoImpl();

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doRequest(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doRequest(request, response);
    }

    private void doRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("/honey");
        String searchQuery = StringUtils.isNotBlank(request.getParameter("query")) ? request.getParameter("query") : "0"; //http://localhost:8080/TestBackEnd_war/FrontController?query=10
        String typeOfSearch = StringUtils.isNotBlank(request.getParameter("type")) ? request.getParameter("type") : "0";
        String userName = StringUtils.isNotBlank(request.getParameter("userName")) ? request.getParameter("userName") : "0";
        try {
            if (dispatcher != null) {
                String result = " ";
                switch (typeOfSearch) {
                    case FIND_ONE:  //http://localhost:8080/TestBackEnd_war/FrontController?query=1&type=findOne
                        result = employeeDao.findOne(Long.parseLong(searchQuery)).getLogin();
                        break;
                    case FIND_BY_ID:  //http://localhost:8080/TestBackEnd_war/FrontController?query=8&type=findById
                        Optional<Employee> optionalEmployee = employeeDao.findById(Long.parseLong(searchQuery));
                        if (optionalEmployee.isPresent()) {
                            result = optionalEmployee.get().getLogin();
                        }
                        break;
                    case SAVE:  //http://localhost:8080/TestBackEnd_war/FrontController?query=33&type=save&userName=Sergey
                        Employee employee = new Employee();
                        employee.setUsername(userName);
                        employee.setAge(25);
                        employee.setLogin(UUID.randomUUID().toString());
                        employee.setPassword(UUID.randomUUID().toString());
                        employee.setBirthDay(Date.valueOf("1998-08-08"));
                        employee.setHavePet(new Boolean(true));
                        result = employeeDao.save(employee).getLogin();
                        break;
                    case UPDATE:  //http://localhost:8080/TestBackEnd_war/FrontController?query=1&type=update
                        Employee employeeForUpdate = employeeDao.findOne(Long.parseLong(searchQuery));
                        employeeForUpdate.setUsername(userName);
                        employeeForUpdate.setBirthDay(Date.valueOf("1988-08-08"));
                        result = employeeDao.update(employeeForUpdate).getLogin();
                        break;
                    case FIND_ALL:  //http://localhost:8080/TestBackEnd_war/FrontController?query=1&type=findAll
                        result = employeeDao.findAll().stream().map(Employee::getLogin).collect(Collectors.joining(","));
                        break;
                    case DELETE:  //http://localhost:8080/TestBackEnd_war/FrontController?query=1&type=delete
                        Employee employeeForDelete = employeeDao.findOne(Long.parseLong(searchQuery));
                        result = employeeDao.delete(employeeForDelete).toString();
                        break;
                    case SEARCH:  //http://localhost:8080/TestBackEnd_war/FrontController?query=1&type=search
                    default:
                        result = employeeDao.search(searchQuery).stream().map(Employee::getLogin).collect(Collectors.joining(","));
                        break;
                }
                request.setAttribute("userNames", result);
                dispatcher.forward(request, response);
            }
        } catch (Exception e) {
            request.setAttribute("errors", e.getMessage());
            dispatcher.forward(request, response);
        }
    }
}





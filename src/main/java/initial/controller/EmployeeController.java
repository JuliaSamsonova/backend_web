package initial.controller;

import initial.domain.Employee;
import initial.service.EmployeeService;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
//
//@Controller
//@RequestMapping("/employees")
//public class EmployeeController{
//    private EmployeeService employeeService;
//
//    public EmployeeController(EmployeeService employeeService){
//        this.employeeService = employeeService;
//    }
//
//@GetMapping
//    public String findAll (ModelMap model) {
////    List<Employee>employees = employeeService.findAll();
//        model.addAttribute("employeess", employeeService.findAll().stream().map(Employee::getLogin).
//                collect(Collectors.joining(",")));
//        return "/WEB-INF/views/employeess";
//}
//
//@GetMapping ("/search")
//    public  String search(@RequestParam("query") String query, ModelMap model){
//        model.addAttribute("employeess", employeeService.search(query));
//        return "/WEB-INF/views/employeess";
//}
//
//
//@GetMapping("/{id}")
//    public  String findById(@PathVariable("id") Long employeeId, ModelMap model) throws EmptyResultDataAccessException {
//        model.addAttribute("employee", employeeService.findOne(employeeId));
//        return "/WEB-INF/views/employee";
//}
//}
package initial.controller.request;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.io.Serializable;
import java.sql.Date;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ApiModel(description = "Model is created for employees")
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class EmployeeCreateRequest /*implements Serializable*/ {


    @NotNull
    @NotEmpty
    @Size(min=2,max=30)
    @ApiModelProperty(notes = "Employee username",required = true, dataType = "string")
    private String username;

    private Integer age;

    private String login;

    private String password;

    private Date birthDay;

    private boolean havePet;

}

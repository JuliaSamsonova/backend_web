package initial.controller;

import initial.dao.EmployeeDao;
import initial.domain.Employee;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.stream.Collectors;


@Controller
public class HoneyController {

    private EmployeeDao employeeDao;

public HoneyController (@Qualifier("employeeRepositoryJDBCTemplate") EmployeeDao employeeDao){
    this.employeeDao=employeeDao;
}

        @GetMapping("/honey")
           public String honey(ModelMap modelMap){
            List<Employee>employee = employeeDao.findAll();
    modelMap.addAttribute("employees", employee.stream().map(Employee::getLogin).collect(Collectors.joining(" , ")));
       return "honey";
    }
}

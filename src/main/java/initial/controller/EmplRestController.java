package initial.controller;

import initial.controller.request.EmployeeCreateRequest;
import initial.domain.Employee;
import initial.service.EmployeeService;
import io.swagger.annotations.*;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(path = "/employees", produces = "application/json")
public class EmplRestController {

        private EmployeeService employeeService;

        private static final Logger log = Logger.getLogger(initial.controller.EmplRestController.class);

        public EmplRestController(EmployeeService employeeService) {
            this.employeeService = employeeService;
        }

    @ApiOperation("Finding all employees")
    @ApiResponses({
            @ApiResponse(code = 200, message = "sucessfully loading employees"),
            @ApiResponse(code = 500, message = "Server error, something is wrong")
    })
        @GetMapping
        public ResponseEntity<List<Employee>> findAll() {
            return new ResponseEntity<>(employeeService.findAll(), HttpStatus.OK);
        }

    @ApiOperation("Searching employee by login")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Successfully loading employees"),
            @ApiResponse(code = 500, message = "Server error, something is wrong")
    })
@ApiImplicitParams({
    @ApiImplicitParam(name = "query", value = "Search query for employee - free text", example = "mike", required = true, dataType = "String", paramType = "query")})
        @GetMapping("/search")
        public List<Employee> search(@RequestParam("query") String query) {
            return employeeService.search(query);
        }

    @ApiOperation("Finding employee by Id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "sucessfully loading employees"),
            @ApiResponse(code = 500, message = "Server error, something is wrong")
    })
    @ApiImplicitParams({
    @ApiImplicitParam(name = "id", value = "Employee dataBase Id", example = "10", required = true, dataType = "long", paramType = "path")})
        @GetMapping("{id}")
        public Employee findById(@PathVariable("id") Long employeeId) {
            return employeeService.findOne(employeeId);
        }

    @ApiOperation("Creation new values for employee")
    @ApiResponses({
            @ApiResponse(code = 201, message = "Successfully loading employees"),
            @ApiResponse(code = 422, message = "Failed user creation properties validation"),
            @ApiResponse(code = 500, message = "Server error, something is wrong")
    })
          @PostMapping
        public Employee save(@Valid @RequestBody EmployeeCreateRequest createRequest) {
            Employee employee = new Employee();
            employee.setUsername(createRequest.getUsername());
            employee.setAge(createRequest.getAge());
            employee.setLogin(createRequest.getLogin());
            employee.setPassword(createRequest.getPassword());
            employee.setBirthDay(createRequest.getBirthDay());
            employee.setHavePet(new Boolean(true));
            return employeeService.save(employee);
        }
    }



